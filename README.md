IOSTemplate,App框架Demo.

App的框架，如大楼之根基，好的框架对于开发，拓展和维护可以起到事半功倍的效果，其重要性不言而喻.

![输入图片说明](http://git.oschina.net/uploads/images/2016/0226/191728_38fa4320_302364.png "在这里输入图片标题")
## Features

- [x] JavaScript call Native App
- [x] Native App call JavaScript

## Requirements

- iOS 8.0+ / Mac OS X 10.9+ / tvOS 9.0+ / watchOS 2.0+
- Xcode 7.1+

## Communication

- 如果您需要帮助，请与我联系hengchengfei@sina.com

## Usage

### App结构如下

  常见的APP框架大多只有一个Project，包括了所有的业务功能实现。
  其缺点是，对于一个稍微大型的APP，结构显得较为复杂，不利于后期的开发和维护。

  以下划分，是我个人的一些理解，不足之处还请指教。
  ※ APP
    大的层级划分
       Classes－－－所有开发文件放在此
           ｜－－－－－Common－－－－本app中常见的定义和操作
           ｜－－－－－Logic－－－－所有业务功能相关文件
       Third－－－所有第三方依赖（不支持cocoapod的）
  
    另外依赖下面的IOSModel，IOSCommon，IOSView三个framework。
   
  ※ Model（Dynamic Framework）
   所有的Http接口模型对象。
   此framework作为Client和Server的桥梁，只需关注接口的数据格式。
   另外包括Json反序列化为Model的过程，以后若要替换第三方，只需修改此framework即可。

  ※ Common（Dynamic Framework）
   通用的操作，如网络请求，Extension等。
  此framework，包含了和业务无关的通用的操作，并对第三方做了再次封装。
  若开发其他app，只需引用此framework即可。

  ※ View（Dynamic Framework）
   自定义的View，如圆角图片等（Storyboard可直接设置，不需要编写代码）

### CocoaPods依赖的常见第三方库
![输入图片说明](http://git.oschina.net/uploads/images/2016/0226/191901_97a1a055_302364.png "在这里输入图片标题")

### 关于IOS9适配问题，此工程中也已经全部解决。
    1：BitCode问题
    2：非Https请求
    3：第三方登录问题
