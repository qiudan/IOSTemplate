//
//  CFSHomeController.swift
//  IOSTemplate
//
//  Created by hengchengfei on 15/11/25.
//  Copyright © 2015年 chengfeisoft. All rights reserved.
//

import UIKit
import IOSModel

class CFSHomeController: UIViewController,UITableViewDataSource,UITableViewDelegate{

    @IBOutlet weak var tableView:UITableView!
    
    var datasource:[HotModel]?
    
    override func viewDidLoad() {
        self.tableView.estimatedRowHeight = 150
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.tableHeaderView = UIView()
        self.tableView.tableFooterView = UIView()
        
        self.title = "首页"
        
        requestData()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
    
    //
    func requestData(){
        cfsRequest(.GET, URLHomeHots,success:{ (statusCode, data, model:BaseModel<HotModel>?) -> Void in
            if statusCode == HttpStatusCode.Normal.rawValue {
                if let data = model?.data {
                    self.datasource = data
                    self.tableView.reloadData()
                }
            }
        })
    }
    
    // MARK: - UITableViewDataSource,UITableViewDelegate
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = datasource?.count{
            return count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let model = datasource?[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("CFSHomeCell", forIndexPath: indexPath) as! CFSHomeCell
        
        cell.selectionStyle = .None
        cell.config(model!)
        
        return cell
    }
}
