//
//  CFSHomeCell.swift
//  IOSTemplate
//
//  Created by hengchengfei on 15/11/25.
//  Copyright © 2015年 chengfeisoft. All rights reserved.
//

import UIKit
import IOSModel
import IOSCommon

class CFSHomeCell: UITableViewCell {
    @IBOutlet weak var headImageView:UIImageView!
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var timeLabel:UILabel!
    @IBOutlet weak var contentLabel:UILabel!
    
    func config(model:HotModel){
        nameLabel.text = model.userName
        contentLabel.text = model.commentContent
        
        headImageView.image = nil
        if let url = model.userIcon where NSURL(string: url) != nil {
            headImageView.cf_setImageWithURL(NSURL(string: url)!, placeHolderImage: nil)
        }
        
        timeLabel.text = nil
        if let t = model.commentTime {
            timeLabel.text = t.toDate().calTimeAfterNow()
        }
    }
    
}
