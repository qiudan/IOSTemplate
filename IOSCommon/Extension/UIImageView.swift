//
//  UIImageView.swift
//  eval
//
//  Created by hengchengfei on 15/9/4.
//  Copyright © 2015年 chengfeisoft. All rights reserved.
//

import UIKit
import SDWebImage

public extension UIImageView {
    public func cf_setImageWithURL(url:NSURL!,placeHolderImage:UIImage!){
        self.sd_setImageWithURL(url, placeholderImage: placeHolderImage)
    }
}
